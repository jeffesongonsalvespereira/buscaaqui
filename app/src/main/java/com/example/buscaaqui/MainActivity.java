package com.example.buscaaqui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.buscaaqui.mobel.Usuario;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;


/*
* add no manifest quando for usar o celular
*  android:debuggable="true"
*
*
* */



public class MainActivity extends AppCompatActivity  {

    Button btn_login, btn_cadastrar;
    RelativeLayout layout_main;

    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Instacia do Firebase
        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
        users = db.getReference("usuarios");

        btn_login = (Button) findViewById(R.id.button_login);
        btn_cadastrar = (Button) findViewById(R.id.button_cadastar);
        layout_main = (RelativeLayout) findViewById(R.id.id_layout_main);

        btn_cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowCadastrar();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showlogin();
            }
        });

    }

    private void showlogin(){

        final AlertDialog.Builder dlog = new AlertDialog.Builder(this);
        dlog.setTitle("Login");

        LayoutInflater inflater = LayoutInflater.from(this);
        View login_layout = inflater.inflate(R.layout.activity_loginemail,null);

        final MaterialEditText editEmail = login_layout.findViewById(R.id.id_email_login);
        final MaterialEditText editSenha = login_layout.findViewById(R.id.id_senha_login);

        dlog.setView(login_layout);

        dlog.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        btn_login.setEnabled(false);


                        if (TextUtils.isEmpty(editEmail.getText().toString())) {
                            Snackbar.make(layout_main, "Insira seu E-mail", Snackbar.LENGTH_SHORT).show();
                            return;
                        }
                        if (TextUtils.isEmpty(editSenha.getText().toString())) {
                            Snackbar.make(layout_main, "Insira seu Senha", Snackbar.LENGTH_SHORT).show();
                            return;
                        }
                        final android.app.AlertDialog esperando = new SpotsDialog(MainActivity.this);
                        esperando.show();

                        auth.signInWithEmailAndPassword(editEmail.getText().toString(),editSenha.getText().toString())
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        esperando.dismiss();
                                        startActivity(new Intent(MainActivity.this,Principal.class));
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                esperando.dismiss();
                                Snackbar.make(layout_main,"Falha no Login"+e.getMessage(),Snackbar.LENGTH_SHORT)
                                        .show();

                                btn_login.setEnabled(true);
                            }
                        });

                    }
                });

        dlog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dlog.show();

    }

    private void ShowCadastrar() {

        final AlertDialog.Builder dlog = new AlertDialog.Builder(this);
        dlog.setTitle("Cadastrar");

        LayoutInflater inflater = LayoutInflater.from(this);
        View cadastrar_layout = inflater.inflate(R.layout.activity_cadastar,null);

        final MaterialEditText editText = cadastrar_layout.findViewById(R.id.id_nome_cadastrar);
        final MaterialEditText editEmail = cadastrar_layout.findViewById(R.id.id_emailCadadastar);
        final MaterialEditText editTelefone = cadastrar_layout.findViewById(R.id.id_telefone_cadastrar);
        final MaterialEditText editSenha = cadastrar_layout.findViewById(R.id.id_senhaCadastar);

        dlog.setView(cadastrar_layout);

        dlog.setPositiveButton("Cadastar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                if (TextUtils.isEmpty(editText.getText().toString())) {
                    Snackbar.make(layout_main,"Insira seu Nome", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(editTelefone.getText().toString())) {
                    Snackbar.make(layout_main,"Insira seu Telefone", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(editEmail.getText().toString())) {
                    Snackbar.make(layout_main,"Insira seu E-mail", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(editSenha.getText().toString())) {
                    Snackbar.make(layout_main,"Insira seu Senha", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                auth.createUserWithEmailAndPassword(editEmail.getText().toString(),editSenha.getText().toString())
                        .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {

                                Usuario user = new Usuario();

                                user.setEmail(editEmail.getText().toString());
                                user.setNome(editText.getText().toString());
                                user.setTelefone(editTelefone.getText().toString());
                                user.setSenha(editSenha.getText().toString());

                                users.child(FirebaseAuth.getInstance().getUid()).setValue(user)
                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {
                                                                    Snackbar.make(layout_main,"Cadastro Efetuado com Sucesso!", Snackbar.LENGTH_SHORT)
                                                                            .show();
                                                                }
                                                            })
                                                                .addOnFailureListener(new OnFailureListener() {
                                                                    @Override
                                                                    public void onFailure(@NonNull Exception e) {
                                                                        Snackbar.make(layout_main,"Falha no Cadastro"+e.getMessage(), Snackbar.LENGTH_SHORT)
                                                                                .show();

                                                                    }
                                                                });

                            }
                        })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Snackbar.make(layout_main,"Falha"+e.getMessage(), Snackbar.LENGTH_SHORT).show();

                                }
                            });
            }
        });

        dlog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dlog.show();
    }


}
